<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}
if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (! function_exists('trans')) {
    function trans($transKey, $fallback = '') {
        if(isset($fallback)) {
            return $fallback;
        }
        return $transKey;
    }
}
/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new App\ApiApplication(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->register(Jenssegers\Mongodb\MongodbServiceProvider::class);
$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/


$app->middleware([
    App\Http\Middleware\RequestLogMiddleware::class
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/


$elasticConfig = require __DIR__.'/../'.'config/elasticsearch.php';
$app['config']->set('elasticsearch.connections', $elasticConfig['connections']);
$app['config']->set('elasticsearch.defaultConnection', $elasticConfig['defaultConnection']);

$sentinelConfig = require __DIR__.'/../'.'config/cartalyst.sentinel.php';
$app[ 'config' ]->set('cartalyst.sentinel', $sentinelConfig);


// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
//Below line makes the segmentation fault when running artisan command for clear cache
//and many other issues.
//TODO need to find the root cause
//$app->register(Illuminate\Cache\CacheServiceProvider::class);

$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(Cartalyst\Sentinel\Laravel\SentinelServiceProvider::class);
$app->register(LaunchCMS\LaunchCoreServiceProvider::class);
$app->register(Cviebrock\LaravelElasticsearch\LumenServiceProvider::class);
$app->register(Dingo\Api\Provider\LumenServiceProvider::class);




$app['Dingo\Api\Exception\Handler']->setErrorFormat([
    'error' => [
        'message' => ':message',
        'errors' => ':errors',
        'code' => ':code',
        'status_code' => ':status_code',
        'debug' => ':debug'
    ]
]);

app('Dingo\Api\Transformer\Factory')->register('LaunchCMS\Models\Content\Field', 'App\Transformer\FieldTransformer');
/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../app/Http/routes.php';
});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->get('/', 'App\Http\Controllers\Api\HomeApiController@index');
    $api->get('content-type', 'App\Http\Controllers\Api\ContentTypeApiController@listing');
    $api->get('content/{contentType}/{contentID}', 'App\Http\Controllers\Api\ContentApiController@detail');
    $api->get('content/{contentType}', 'App\Http\Controllers\Api\ContentApiController@listing');
});

return $app;
