<?php

class ApiIndexTest extends \ApiTestCase
{
    /** @test **/
    public function index_status_code_should_be_200()
    {
        $this->get('/api')->seeStatusCode(200)->
            seeJson([
             'title' => 'Welcome to LaunchCMS API',
             'version' => '1.0'
            ]);
    }
}
