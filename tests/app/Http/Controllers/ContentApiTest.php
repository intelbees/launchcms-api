<?php

use LaunchCMS\Models\Content\StringField;
use LaunchCMS\Models\Content\DataObject\Content;
use Illuminate\Support\Facades\Schema;

class ContentApiTest extends \ApiTestCase
{
    protected function initData()
    {
        $this->structureService->createContentType(['name' => 'Book', 'alias' => 'book']);
        $introField = new StringField();
        $introField->setName('Introduction');
        $introField->setAlias('introduction');
        $this->structureService->addField('book', $introField);
    }
    protected function createSampleBook($title, $intro) {
        $content = new Content();
        $content->name = $title;
        $content->introduction = $intro;
        return $this->contentService->saveContent('book', $content);
    }

    protected function resetData()
    {
        Schema::drop('book');
        Schema::drop('cms_content_types');
    }


    /** @test **/
    public function view_content_detail_should_throw_404_when_content_does_not_exist()
    {
        $this->get('/api/content/book/content_id_not_exist')->seeStatusCode(404);
    }

    /** @test **/
    public function view_content_detail_should_throw_404_when_content_in_trash()
    {
        $bookID = $this->createSampleBook('Clean code', 'Good book for developer');
        $this->contentService->moveToTrash('book', $bookID);
        $this->get('/api/content/book/'.$bookID)->seeStatusCode(404);
    }

    /** @test **/
    public function view_content_detail_should_throw_400_when_content_type_does_not_exist()
    {
        $this->get('/api/content/content_type_does_not_exist/123')->seeStatusCode(400);
    }

    /** @test **/
    public function view_content_detail_should_return_data_if_exist()
    {
        $bookID = $this->createSampleBook('Clean code', 'Good book for developer');
        $response = $this->get('/api/content/book/'.$bookID);
        $response->seeStatusCode(200)->seeJson([
            'name' => 'Clean code'
        ]);

    }

    /** @test **/
    public function list_content_should_return_data_if_exist() {
        $this->createSampleBook('Clean code', 'Good book for developer');

        $response = $this->get('/api/content/book');
        $response->seeStatusCode(200)->seeJson([
            'name' => 'Clean code'
        ]);
    }


    /** @test **/
    public function list_content_should_not_return_content_in_trash() {
        $bookID = $this->createSampleBook('Clean code', 'Good book for developer');
        $this->contentService->moveToTrash('book', $bookID);
        $this->get('/api/content/book');
        $data = json_decode($this->response->getContent(), true);
        $this->assertEmpty($data['data']);
    }

    /** @test **/
    public function list_content_should_return_data_with_name_field_and_meta_data_only_by_default() {
        $this->createSampleBook('Clean code', 'Good book for developer');
        $response = $this->get('/api/content/book');
        $response->seeStatusCode(200)->seeJson([
            'name' => 'Clean code'
        ]);

        $data = json_decode($this->response->getContent(), true);
        $metaKeys = ['content_type_id', 'created_at', 'updated_at', 'content_type_path'];

        foreach ($metaKeys as $key) {
            $this->assertArrayHasKey($key, $data['data'][0]);
        }
        $this->assertArrayNotHasKey('introduction', $data['data'][0]);
    }

    /** @test **/
    public function list_content_should_return_data_with_fields_in_fields_params() {
        $this->createSampleBook('Clean code', 'Good book for developer');
        $response = $this->get('/api/content/book?fields=introduction');
        $response->seeStatusCode(200)->seeJson([
            'name' => 'Clean code'
        ]);
        $data = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('introduction', $data['data'][0]);
    }
}
