<?php
use Illuminate\Support\Facades\Cache;
abstract class ApiTestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * @var \LaunchCMS\Services\Interfaces\StructureServiceInterface
     */
    protected $structureService;

    /**
     * @var \LaunchCMS\Services\Interfaces\UserServiceInterface
     */
    protected $userService;

    /** @var  \LaunchCMS\Services\Interfaces\ContentServiceInterface */
    protected $contentService;

    /** @var  \LaunchCMS\Services\Interfaces\OrganizationServiceInterface */
    protected $organizationService;
    
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function setUp()
    {
        parent::setUp();
        $this->structureService = $this->app['StructureService'];
        $this->contentService = $this->app['ContentService'];
        $this->userService = $this->app['UserService'];
        $this->organizationService = $this->app['OrganizationService'];
        $this->initData();
    }

    public function tearDown()
    {
        $this->resetData();
        Cache::flush();
        parent::tearDown();
    }

    protected function initData() {

    }
    protected function resetData() {

    }

}
