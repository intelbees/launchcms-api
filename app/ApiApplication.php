<?php

namespace App;
use Laravel\Lumen\Application;

class ApiApplication extends Application
{
    const VERSION = '5.3';
}