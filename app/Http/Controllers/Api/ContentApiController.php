<?php

namespace App\Http\Controllers\Api;


use App\Transformer\CollectionTransformer;
use App\Transformer\ContentTransformer;
use Illuminate\Http\Request;
use Jenssegers\Mongodb\Query\Builder;
use LaunchCMS\Models\Content\DataObject\Content;
use LaunchCMS\Services\Exceptions\CMSServiceException;
use LaunchCMS\Services\Interfaces\ContentServiceInterface;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Collection;



class ContentApiController extends Controller
{
    const DEFAULT_FIELDS = ['_id', 'name', 'content_type_id', 'created_at', 'updated_at', 'content_type_path'];
    const ALL_FIELDS = 'allFields';

    const FIELDS = 'fields';

    const EXTRACTED_FIELDS = 'extractedFields';

    protected function convertContentArrayToCollection(array $contentData) {
        $contentItemArray = array();
        foreach ($contentData as $contentItem) {
            $content = Content::fromArray($contentItem);
            $contentItemArray[] = $content;
        }
        return new Collection($contentItemArray);
    }

    protected function extractFieldArrayFromRequest(Request $request) {
        $fieldParams = '';
        if($request->has(self::FIELDS)) {
            $fieldParams = $request->input(self::FIELDS);
            $fieldParams = trim($fieldParams);
        }
        $extractedFields = [];
        if($fieldParams === '*') {
            return [ self::ALL_FIELDS => true, self::EXTRACTED_FIELDS => []];
        }
        $rawFieldArray = explode(',', $fieldParams);
        foreach ($rawFieldArray as $field) {
            $field = trim($field);
            if(!empty($field)) {
                $extractedFields[] = $field;
            }
        }
        return [ self::ALL_FIELDS => false, self::EXTRACTED_FIELDS => $extractedFields];
    }

    public function listing(Request $request, $contentTypeAlias) {

        /** @var StructureServiceInterface $structureService */
        $structureService = app('StructureService');
        $contentType = $structureService->getContentTypeByAlias($contentTypeAlias);

        $projectionFields = self::DEFAULT_FIELDS;

        if(empty($contentType)) {
            throw new NotFoundHttpException('Content type not found');
        }
        $extractedResult = $this->extractFieldArrayFromRequest($request);
        if(!$extractedResult[ self::ALL_FIELDS ]) {
            $projectionFields = array_merge($extractedResult[ self::EXTRACTED_FIELDS ], $projectionFields);
        }

        /** @var ContentServiceInterface $contentService */
        $contentService = app('ContentService');
        /** @var Builder $queryBuilder */
        $queryBuilder = $contentService->getQueryBuilder($contentTypeAlias);
        if(!$extractedResult[ self::ALL_FIELDS ]) {
            $projectionParams = [];
            foreach ($projectionFields as $field) {
                $projectionParams[$field] = 1;
            }
            $queryBuilder = $queryBuilder->project($projectionParams);
        }
        $contentData = $queryBuilder->where('trashed', false)->get();
        $contentTransformer = new ContentTransformer($contentType);
        return $this->response->collection($this->convertContentArrayToCollection($contentData), $contentTransformer);
    }

    public function detail($contentType, $contentID) {
        /** @var ContentServiceInterface $contentService */
        $contentService = app('ContentService');
        $queryBuilder = null;
        try {
            $queryBuilder = $contentService->getQueryBuilder($contentType);
        } catch (CMSServiceException $ex) {
            throw new BadRequestHttpException($ex->getTranslatedMessage());
        }

        $contentData = $queryBuilder->where('_id', $contentID)->where('trashed', false)->first();
        if(empty($contentData)) {
            throw new NotFoundHttpException('Content not found');
        }
        return $contentData;
    }
}
