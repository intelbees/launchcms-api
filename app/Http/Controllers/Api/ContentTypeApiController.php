<?php

namespace App\Http\Controllers\Api;


use App\Transformer\CollectionTransformer;
use App\Transformer\ContentTypeTransformer;
use LaunchCMS\Services\Interfaces\StructureServiceInterface;

class ContentTypeApiController extends Controller
{
    public function listing() {
        /** @var StructureServiceInterface $structureService */
        $structureService = app('StructureService');
        $contentTypes = $structureService->getAllContentTypes();
        return $this->response->collection($contentTypes, new ContentTypeTransformer());
    }
}
