<?php

namespace App\Http\Controllers\Api;


class HomeApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index() {
        return [
            'title' => 'Welcome to LaunchCMS API',
            'version' => '1.0'
        ];
    }
}
