<?php

namespace App\Transformer;

use LaunchCMS\Models\Content\ContentType;
use League\Fractal;
class ContentTypeTransformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'fields'
    ];

    public function transform(ContentType $contentType)
    {
        return [
            'id'      => (string) $contentType->id,
            'name'   => $contentType->getName(),
            'alias' => $contentType->getAlias(),
            'description' => $contentType->getDescription(),
            'created_at' => $contentType->created_at->toIso8601String()
        ];
    }

    /**
     * Include Author
     *
     * @param Book $book
     * @return \League\Fractal\Resource\Item
     */
    public function includeFields(ContentType $contentType)
    {
        $fields = $contentType->getAllFields();

        return $this->collection($fields, new FieldTransformer());
    }
}