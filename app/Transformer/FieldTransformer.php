<?php

namespace App\Transformer;

use LaunchCMS\Models\Content\Field;
use League\Fractal;
class FieldTransformer extends Fractal\TransformerAbstract
{
    public function transform(Field $field)
    {
        return [
            'id'      => (string) $field->id,
            'name'   => $field->getName(),
            'alias' => $field->getAlias(),
            'data_type' => $field->getDataType(),
            'allow_full_text_search' => $field->allowFullTextSearch(),
            'unique' => $field->isUnique(),
            'indexable' => $field->index(),
            'required' => $field->isRequired(),
            'extra_data_type_info' => $field->getExtraDataTypeInfo()

        ];
    }
}