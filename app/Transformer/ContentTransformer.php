<?php

namespace App\Transformer;

use Carbon\Carbon;
use LaunchCMS\Models\Content\ContentType;
use LaunchCMS\Models\Content\DataObject\Content;
use League\Fractal;
class ContentTransformer extends Fractal\TransformerAbstract
{

    /** @var  ContentType */
    protected $contentType;


    public function __construct(ContentType $contentType)
    {
        $this->contentType = $contentType;
    }

    public function transform(Content $content)
    {
        $data = [
            'id'      => $content->id,
            'name'   => $content->name,
            'content_type_id' => $content->contentTypeID,
            'content_type_path' => $content->contentTypePath,
            'created_at' => $content->createdTime->toIso8601String(),
            'updated_at' => $content->lastUpdatedTime->toIso8601String()
        ];
        foreach ($content->attributes as $key => $value) {
            if($value instanceof Carbon) {
                $data[$key] = $value->toIso8601String();
            } else {
                $data[$key] = $value;
            }
        }
        return $data;
    }

}